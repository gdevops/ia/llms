
.. _intro_projets_ia_2023:

===================================
Introduction Projets IA 2023
===================================


2023-09-13 Des giga-modèles pour le traitement automatique du langage naturel et des données multimodales
==============================================================================================================

- https://www.ins2i.cnrs.fr/fr/cnrsinfo/des-giga-modeles-pour-le-traitement-automatique-du-langage-naturel-et-des-donnees
- https://anr.fr/fileadmin/aap/2023/aap-tsia-2023.pdf
- https://anr.fr/fileadmin/aap/2023/selection/tsia-selection-2023.pdf
- https://www.univ-nantes.fr/decouvrir-luniversite/vision-strategie-et-grands-projets/malades-grands-modeles-de-langue-adaptables-et-souverains-pour-le-domaine-medical-francais
- http://www.ls2n.fr/
- https://www.liglab.fr/fr


Pour répondre aux enjeux scientifiques et sociétaux soulevés par
l’émergence des grands modèles de langue, l’Agence nationale de la
recherche (ANR) a lancé en février 2023 `l’appel à projet Thématiques
Spécifiques en Intelligence Artificielle (TSIA) <https://anr.fr/fileadmin/aap/2023/aap-tsia-2023.pdf>`_

`Huit projets sont lauréats <https://anr.fr/fileadmin/aap/2023/selection/tsia-selection-2023.pdf>`_ pour la thématique « Giga-modèles pour le
traitement automatique du langage naturel et des données multimodales »,
dont le projet `MALADES <https://www.univ-nantes.fr/decouvrir-luniversite/vision-strategie-et-grands-projets/malades-grands-modeles-de-langue-adaptables-et-souverains-pour-le-domaine-medical-francais>`_ coordonné par Richard Dufour, professeur à Nantes
Université, membre du `Laboratoire des Sciences du Numérique de Nantes <http://www.ls2n.fr/>`_
(LS2N - CNRS/Centrale Nantes/Nantes Université) et Pantagruel, coordonné
par Didier Schwab, professeur à l’Université Grenoble Alpes, `membre du
Laboratoire d’Informatique de Grenoble (LIG-CNRS/Université Grenoble Alpes) <https://www.liglab.fr/fr>`_

L'adoption par la société des outils issus du traitement automatique du
langage naturel (TALN), comme ChatGPT d'OpenAI, `Bard de Google <https://bard.google.com/chat>`_
ou de la solution open-source Alpaca de Stanford s'est accélérée ces dernières années.

Cela est dû à leur amélioration constante : ils bénéficient de la rencontre
d'approches statistiques par apprentissage profond (deep learning), de la
disponibilité d'énormes quantité de données (Big Data) et de l'accès à
d'importantes puissances de calcul.

Grâce à des milliards de documents textuels collectés et à des stratégies
d'apprentissage non-supervisé efficaces, les derniers modèles, connus
sous le nom de grands modèles de langue (Large Language Models ou LLM),
intègrent notamment des capacités génératives.

Cela signifie qu'ils sont aussi capables de générer du contenu textuel.
Ils peuvent ainsi être adaptés pour traiter une multitude de tâches
de TALN, comme de la traduction automatique, le résumé de documents, le
dialogue humain-machine, etc.
Bien qu'une nouvelle étape ait été franchie en TALN, ces modèles soulèvent
encore d'importants enjeux scientifiques et sociétaux.
