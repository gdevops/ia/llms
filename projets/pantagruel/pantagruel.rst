.. index::
   ! Pantagruel

.. _pantagruel:

===================================
**Pantagruel**
===================================

- https://www.ins2i.cnrs.fr/fr/cnrsinfo/des-giga-modeles-pour-le-traitement-automatique-du-langage-naturel-et-des-donnees
- https://anr.fr/fileadmin/aap/2023/aap-tsia-2023.pdf
- https://anr.fr/fileadmin/aap/2023/selection/tsia-selection-2023.pdf
- https://www.liglab.fr/fr


Description
=============

Pour répondre aux enjeux scientifiques et sociétaux soulevés par
l’émergence des grands modèles de langue, l’Agence nationale de la
recherche (ANR) a lancé en février 2023 `l’appel à projet Thématiques
Spécifiques en Intelligence Artificielle (TSIA) <https://anr.fr/fileadmin/aap/2023/aap-tsia-2023.pdf>`_

`Huit projets sont lauréats <https://anr.fr/fileadmin/aap/2023/selection/tsia-selection-2023.pdf>`_ pour la thématique « Giga-modèles pour le
traitement automatique du langage naturel et des données multimodales »,
dont le projet `MALADES <https://www.univ-nantes.fr/decouvrir-luniversite/vision-strategie-et-grands-projets/malades-grands-modeles-de-langue-adaptables-et-souverains-pour-le-domaine-medical-francais>`_ coordonné par Richard Dufour, professeur à Nantes
Université, membre du `Laboratoire des Sciences du Numérique de Nantes <http://www.ls2n.fr/>`_
(LS2N - CNRS/Centrale Nantes/Nantes Université) et Pantagruel, coordonné
par Didier Schwab, professeur à l’Université Grenoble Alpes, membre du
Laboratoire d’Informatique de Grenoble (LIG-CNRS/Université Grenoble Alpes).


**Pantagruel**, coordonné par Didier Schwab, professeur à l’Université Grenoble
Alpes, :ref:`membre du LIG <liglab>`, considère pleinement la problématique multimodale,
mais également inclusive, pour le français sous forme écrite, orale, ou
de pictogrammes.

Pantagruel vise à développer des modèles autosupervisés librement accessibles
pour le français, comprenant une à trois des modalités pour les domaines
généraux et cliniques.

**Le projet concevra également des bancs d’essais permettant d'évaluer la
capacité de généralisation de ce type de modèles**.

Contact
=========

- Didier Schwab, Professeur à l'Université de Grenoble Alpes, membre du LIG
  didier.schwab@univ-grenoble-alpes.fr
