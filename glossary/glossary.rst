.. index::
   ! Glossaire

.. _glossary:

========================================
Glossary
========================================

.. glossary::


  algogen
      A catch-all term for algorithmically-generated art and text.


  Machine Learning Engineers

      ML engineers are focused on practically implementing and deploying
      machine learning models in real-world applications.

      Their main goal is to build and optimize machine learning systems
      that can be integrated into products and services.

      They are skilled in software development, data engineering, and
      machine learning frameworks.

      Responsibilities of ML Engineers:

        - Data Pipelines: ML engineers can clean and prepare data for training
          machine learning models, although there are also dedicated
          data engineers these days !
        - Model Deployment: ML engineers deploy trained models into production
          environments, making them accessible to end-users or applications.
        - Scaling and Optimization: They optimize the performance and
          efficiency of machine learning systems to handle large-scale data and real-time processing.
        - Software Integration: ML engineers integrate machine learning
          capabilities into existing software systems or develop new applications.

      Example from Meteorological Data: An ML engineer working with
      meteorological data might take a machine learning model to predict
      rainfall based on historical weather data.

      They would develop a pipeline for the data and deploy it as part
      of a weather forecasting application.

  Grands modèles de langue
  Large Language Models
  LLM
      Grâce à des milliards de documents textuels collectés et à des stratégies
      d'apprentissage non-supervisé efficaces, les derniers modèles, connus
      sous le nom de grands modèles de langue (Large Language Models ou LLM),
      intègrent notamment des capacités génératives.

      Cela signifie qu'ils sont aussi capables de générer du contenu textuel.
      Ils peuvent ainsi être adaptés pour traiter une multitude de tâches
      de TALN, comme de la traduction automatique, le résumé de documents, le
      dialogue humain-machine, etc.
      Bien qu'une nouvelle étape ait été franchie en TALN, ces modèles soulèvent
      encore d'importants enjeux scientifiques et sociétaux.

  ML Scientists:

      ML scientists, on the other hand, are more focused on the research
      and theoretical aspects of machine learning.
      They delve into the development and improvement of algorithms and
      methodologies.
      They conduct experiments, analyze results, and contribute to advancing
      machine learning theory.

      Responsibilities of ML Scientists:

      - Algorithm Development: ML scientists work on creating novel
        machine learning algorithms or improving existing ones to solve complex problems.
      - Research and Experimentation: They design experiments to test the effectiveness of different algorithms and approaches on various datasets.
      - Performance Evaluation: ML scientists analyze the performance of machine learning models, including their strengths and limitations.
      - Publications: They often publish their research findings in academic journals or present them at conferences to share knowledge with the community.
      - Advancing the Field: ML scientists contribute to the theoretical foundations of machine learning, pushing the boundaries of what is possible with technology.

      Example from Meteorological Data: An ML scientist working with
      meteorological data might explore novel neural network architectures
      or optimization algorithms to improve the accuracy and efficiency
      of weather prediction models.
      They would conduct experiments to evaluate the performance of these
      new methods and publish their findings to benefit the machine
      learning research community.
