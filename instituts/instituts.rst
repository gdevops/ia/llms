
.. _instituts:

===================================
Instituts
===================================

.. toctree::
   :maxdepth: 3

   miai-grenoble-alpes-institute/miai-grenoble-alpes-institute
   laboratoire-d-informatique-de-grenoble/laboratoire-d-informatique-de-grenoble
