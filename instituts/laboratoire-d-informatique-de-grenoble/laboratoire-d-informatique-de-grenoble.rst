.. index::
   ! Laboratoire d'Informatique de Grenoble

.. _liglab:

=================================================
**Laboratoire d'Informatique de Grenoble**
=================================================

- https://www.liglab.fr/fr
- https://www.liglab.fr/fr/annuaire-lig/equipes-recherche
