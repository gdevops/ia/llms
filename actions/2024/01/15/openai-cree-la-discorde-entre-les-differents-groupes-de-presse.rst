.. index::
   pair: François Saltiel;  L’intelligence artificielle tente d’infiltrer la presse française (2024-01-15)
   ! François Saltiel

.. _saltiel_2024_01_15:

==============================================================================================================
2024-01-15 **L’intelligence artificielle tente d’infiltrer la presse française** par François Saltiel
==============================================================================================================

- https://www.radiofrance.fr/franceculture/podcasts/planete-connectee
- https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/openai-cree-la-discorde-entre-les-differents-groupes-de-presse-9009291

Difficile pour les patrons de presse d'appréhender les grandes mutations
technologiques. Ne manquait plus que l'IA, qui s'engouffre dans la brèche et
tante de nouer de nouveaux accords avec les différents acteurs du secteur.

Face à Chat GPT, qui se mue de plus en plus en nouveau moteur de recherche,
vous avez trois possibilités : résister en attaquant, rester neutre et
voir son contenu aspiré ou se rallier et tenter de négocier avec l’ogre
américain. Cette semaine les représentants des éditeurs de presse français
ont rendez-vous avec le comité interministériel sur l’IA initié par
Matignon.

Ils cherchent à alerter les pouvoirs publics sur leurs situations. Avec les
centaines d’articles crées par jour, les éditeurs veulent avoir leur part
du gâteau. Ils se considèrent comme une matière première, un ingrédient
incontournable de l’industrie de l’IA pour offrir une information vérifiée
mais, problème, ils ne pèsent pas bien lourd dans l’écosystème.

L’idée est donc de jouer à la fois la carte collective pour négocier des
accords sectoriels mais également d’entamer pour les groupes puissants,
des deals de gré à gré. Pour l’instant beaucoup de médias comme TF1,
Le Monde ou Radio France ont opté pour le droit d’opt-out et refusent
l’accès à leurs contenus. Une manière de gagner du temps.

Iront-ils jusqu’à suivre l’exemple du groupe de presse, le plus puissant
au monde, Le New York Times qui en décembre a attaqué Open AI pour violation
de droits d’auteurs ? Ce n’est pas évident… Sachant que cette attaque,
pourrait n’être qu’un coup de pression pour mieux négocier un futur
partenariat. D’ailleurs, Open AI a révélé être en négociations avec le
New York Times avant l’action en justice.

Pour la start-up, le journal ne raconte pas toute l’histoire et le New York
Times aurait attaqué à la suite d’échanges commerciaux infructueux.

En Europe, le groupe de presse allemand Springer a noué un accord inédit avec Chat GPT
========================================================================================

La galaxie Axel Springer, c’est entres autres le tabloïd hyper puissant
Bild, le quotidien plus sérieux Die Welt ou des sites influents comme Business
Insider ou Politico.

Mi-décembre 2023, le groupe a tapé dans la main de Chat GPT, ils ont
obtenu de l’argent et des liens qui redirigent les utilisateurs vers
leurs articles.
**Chat GPT peut, de son côté, exploiter l’ensemble du contenu,
archives comprises, et entraine abondamment leur modèle de langage**.

Mais il y a un second volet à cet accord, un arrangement plus polémique.

Des expérimentations sont en cours dans les services du groupe de presse
berlinois. Chat GPT est utilisé au quotidien dans la pratique journalistique :
corrections, mise en page, traduction.
Le groupe justifie cet usage de l’IA pour se concentrer sur le journalisme
de scoop et d’investigation.

**Le directeur du groupe a évoqué une révolution en assurant que l’IA pouvait
soutenir et même remplacer le journaliste**.

L’objectif est aussi de rassurer le marché en promettant de réaliser 100 000 euros
de bénéfice sur trois ans.

En France, une expérience d’intégration de Chat GPT est actuellement
en cours.

**Cela se passe à l’Est Républicain, sous l’œil vigilant et inquiet des
syndicats de journalistes qui ne semblent pas apprécier ce nouveau collègue
de rédaction, qui fragilise un peu plus le métier**.


