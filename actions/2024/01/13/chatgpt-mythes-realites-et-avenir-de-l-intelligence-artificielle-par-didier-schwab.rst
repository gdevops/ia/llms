.. index::
   pair: ChatGPT:  mythes réalités et avenir de l'intelligence artificielle; 2024-01-13
   ! Didier Schwab

.. _schwab_2024_01_13:

=====================================================================================================
2024-01-13 **ChatGPT:  mythes réalités et avenir de l'intelligence artificielle** par Didier Schwab
=====================================================================================================

- https://cinevod.bm-grenoble.fr/v/S1QT2 (ChatGPT : mythes, réalités et avenir de l'Intelligence Artificielle saison 2024 | épisode 2)
- https://www.youtube.com/@didschwab/videos
- https://www.youtube.com/watch?v=IsTLXO82FJs&ab_channel=BlackSheepStudio (Didier SCHWAB, "Les petits pictogrammes font les grands discours")
- https://www.youtube.com/watch?v=yyFZMDl7rPs&ab_channel=ConnectVirtualEventPlatform (DDN - Accélérer la Recherche avec L'AI - University Grenoble - Didier Schwab)
- https://www.youtube.com/watch?v=csz7JhYN27A&ab_channel=IAPau (2023-06-23, ChatGPT : Debunk et application - MEETUP)


Présentation
================

- https://numotheque.grenoblealpesmetropole.fr/selection/chatgpt-mythes-realites-avenir-lintelligence-artificielle

La bibliothèque Kateb Yacine de Grenoble vous invite à une conférence
par Didier Schwab, maitre de conférences à l’Université Grenoble Alpes,
samedi 13 janvier 2024 à 16h, dans le cadre du cycle "Tout comprendre".

Les intelligences artificielles (IA) sont des systèmes informatiques
conçues pour simuler des processus cognitifs humains et exécuter des tâches
intelligentes.

Découvrez comment les IA apprennent à imiter le génie humain, produisant
des résultats aussi surprenants que novateurs. Par leur capacité à créer
du texte, de l'art, de la musique et même des simulations réalistes, ces
intelligences artificielles génératives suscitent l'émerveillement en
ouvrant des portes insoupçonnées dans le monde de la création.

Cependant, ces avancées soulèvent des questions éthiques et juridiques.

La responsabilité de l'IA dans la création de contenu controversé ou trompeur
pose des défis importants en matière de régulation.

ChatGPT:  mythes réalités et avenir de l'intelligence artificielle
=======================================================================

- :ref:`miai`
- :ref:`liglab`

.. figure:: images/slide_1.png


Panorama des grands modèles de langues
================================================================

.. figure:: images/panorama_1.png


Panorama des IA génératives
================================================================

.. figure:: images/panorama.png

   https://www.rapidops.com


Traitement automatique du langage
================================================================

- https://www.nlplanet.org/

.. figure:: images/taln.png

   https://www.nlplanet.org/


The Natural Language Processing Community
--------------------------------------------

- https://www.nlplanet.org/

NLPlanet is a niche community about Natural Language Processing whose
goal is to connect NLP enthusiasts and provide high-quality learning content


.. figure:: images/taln_planet.png

   https://www.nlplanet.org/

Autoapprentissage : objectifs bidirectionnels versus causaux
================================================================

.. figure:: images/causaux_et_bidirectionnels.png

Principes de chatGPT
================================================================

.. figure:: images/principes.png


Applications
================================================================

.. figure:: images/applications.png


Limites éthiques
==================

- :ref:`pantagruel`

.. figure:: images/limites_ethiques.png


Liens
======

- https://bard.google.com/chat
- :ref:`projets`
