

.. _evenements_2024_01:

=======================================
2024-01
=======================================

.. toctree::
   :maxdepth: 3

   15/15
   13/13


2024-01-12 L'actu des intelligences artificielles 🤖 (Numérama)
===================================================================

- https://urlr.me/b2xHM

✍️ Midjourney v6 : des progrès pour afficher du texte dans les images, mais encore des ratés à corriger
-----------------------------------------------------------------------------------------------------------

- https://www.numerama.com/tech/1602286-petit-a-petit-le-charabia-de-midjourney-devient-du-texte-intelligible.html


Malgré des avancées depuis les premières versions, Midjourney lutte encore
pour générer du texte sans erreurs.

La version 6 de cette IA est meilleure dans ce domaine.

Cependant, les résultats sont toujours imparfaits, et le laboratoire américain
reste concentré sur l'amélioration des rendus.


2024-01-11 🌱 The Inner Dev 2024 predictions
=============================================

- https://opsindev.news/
- https://buttondown.email/opsindev.news/archive/2024-01-11-2024-predictions-ollama-mixtral8x7b/

👋 Hey, lovely to see you again

Hope you had a relaxing time, and come refreshed and prepared for an
exciting 2024.

Next month, this newsletter turns 2 years old.

Thanks for reading and engaging with the content and my many thoughts.

Learning AI/ML in 2023 was the hardest challenge in my career, and my
newsletter immensely helped me research, read, and reflect.

I hope you learned new insights, too. :)

Tip: You can use the `web archive search for all things AI <https://opsindev.news/>`_.
