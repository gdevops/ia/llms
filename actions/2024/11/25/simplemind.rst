.. index::
   pair: Kenneth Reitz; Simplemind: AI for Humans

.. _simple_mind_2024_11_25:

================================================================
2024-11-25 **Simplemind: AI for Humans™** by Kenneth Reitz 
================================================================

- https://github.com/kennethreitz/simplemind
- https://github.com/kennethreitz


Python API client for AI providers that intends to replace LangChain and 
LangGraph for most common use cases.

Simplemind is AI library designed to simplify your experience with AI APIs 
in Python. 

Inspired by a "for humans" philosophy, it abstracts away complexity, 
giving developers an intuitive and human-friendly way to interact with 
powerful AI capabilities.
