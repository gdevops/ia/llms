.. index::
   pair: Simon Willison; Things we learned about LLMs in 2024 (2024-12-31)

.. _simon_willison_2023_12_31:

=======================================================================
2024-12-31 **Things we learned about LLMs in 2024** by Simon Willison
=======================================================================

- https://simonwillison.net/2024/Dec/31/llms-in-2024/
- https://simonwillison.net/2024/Dec/31/llms-in-2024/#everything-tagged-llms-on-my-blog-in-2024
- https://huggingface.co/spaces/reach-vb/2024-ai-timeline

A lot has happened in the world of Large Language Models over the course of 
2024. Here’s a review of things we figured out about the field in the 
past twelve months, plus my attempt at identifying key themes and pivotal moments.

This is a sequel to my review of 2023.
