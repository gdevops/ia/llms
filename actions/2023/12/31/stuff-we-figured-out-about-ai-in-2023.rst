.. index::
   pair: Simon Willison; Stuff we figured out about AI in 2023 (2023-12-31)


.. _simon_willison_stuff_2023_12_31:

========================================================================
2023-12-31 **Stuff we figured out about AI in 2023** by Simon Willison
========================================================================

- https://simonwillison.net/2023/Dec/31/ai-in-2023/
- https://nhlocal.github.io/AiTimeline/#2023

2023 was the breakthrough year for Large Language Models (LLMs). 

I think it’s OK to call these AI—they’re the latest and (currently) most 
interesting development in the academic field of Artificial Intelligence 
that dates back to the 1950s.
