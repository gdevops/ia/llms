


.. _lucie_dataset_2025_01_15:

==============================================
2025-01-15 **Lucie Training Dataset Card**
==============================================

- https://huggingface.co/datasets/OpenLLM-France/Lucie-Training-Dataset

The Lucie Training Dataset is a curated collection of text data in English, 
French, German, Spanish and Italian culled from a variety of sources 
including: web data, video subtitles, academic papers, digital books, newspapers, 
and magazines, some of which were processed by Optical Character Recognition (OCR). 

It also contains samples of diverse programming languages.

The Lucie Training Dataset was used to pretrain Lucie-7B, a foundation LLM 
with strong capabilities in French and English. 

Code for data preparation can be found in the training respository for Lucie-7B. 

Due to the licenses of a few subcorpora, the Lucie Training Dataset is 
released under a CC BY-NC-SA 4.0. 

A subset available for commercial use will be released soon.
