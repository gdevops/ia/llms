.. index::
   pair: IA; 50 termes clés de l'intelligence artificielle (2025-01-10)

.. _ia_2025_01_10:

====================================================================
2025-01-10 **50 termes clés de l'intelligence artificielle**
====================================================================

- https://www.culture.gouv.fr/Thematiques/langue-francaise-et-langues-de-france/Agir-pour-les-langues/Moderniser-et-enrichir-la-langue-francaise/Nos-publications/50-termes-cles-de-l-intelligence-artificielle
- https://www.culture.gouv.fr/content/download/365680/pdf_file/50_termes_cl%C3%A9s_de_l%27IA-2025.pdf?inLanguage=fre-FR&version=2

À un mois du Sommet pour l’action sur l’intelligence artificielle (IA), qui 
se tiendra à Paris les 10 et 11 février 2025, la DGLFLF compile dans ce 
recueil 50 termes français recommandés, du terme « intelligence artificielle » 
paru en 1989 aux termes de l'IA générative publiés en décembre 2024. 

Ces notions et leur définition sont enrichies d’équivalents dans d'autres 
langues (anglais, arabe, basque, catalan, espagnol, italien, néerlandais) 
fournis par des institutions partenaires.
