.. index::
   pair: Large Language Models; LLMs
   ! Grands modèles de langue
   ! LLMs

.. 💩

|FluxWeb| `RSS <https://gdevops.frama.io/ia/llms/rss.xml>`_

.. _llms:

=============================================================
**LLMs (Large Language Models/Grands modèles de langue)**
=============================================================

- https://en.wikipedia.org/wiki/Large_language_model
- https://fr.wikipedia.org/wiki/Grand_mod%C3%A8le_de_langage
- https://fr.wikipedia.org/wiki/Mod%C3%A8le_de_langage

.. toctree::
   :maxdepth: 6

   actions/actions
   projets/projets
   instituts/instituts
   models/models
   glossary/glossary
